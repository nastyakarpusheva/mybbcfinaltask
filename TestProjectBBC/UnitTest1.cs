using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace TestProjectBBC
{
    [TestClass]
    public class UnitTest1
    {
        private ReadOnlyCollection<IWebElement> IWebElement;
        IWebDriver driver = new ChromeDriver();
        

        [TestMethod]
        public void TestMethod1()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");

            Thread.Sleep(400);
            IWebElement newsButton = driver.FindElement(By.XPath("//a[text()='News']"));
            newsButton.Click();

            IWebElement headlineArticle = driver.FindElement(By.XPath("//h3[@class='gs-c-promo-heading__title gel-paragon-bold nw-o-link-split__text']"));
            string headlineText = headlineArticle.Text; 
            Assert.AreEqual("Congress narrowly averts US government shutdown", headlineText);
           
            driver.Quit();
        }
        [TestMethod]
        public void TestMethod2()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");

            Thread.Sleep(1000);
            IWebElement newsButton = driver.FindElement(By.XPath("//a[text()='News']"));
            newsButton.Click();

            IList<IWebElement> actualArticles = new List<IWebElement>();
            actualArticles = driver.FindElements(By.XPath("//p[@class='gs-c-promo-summary gel-long-primer gs-u-mt nw-c-promo-summary']"));

            List<string> jbk = new List<string>();
           
            foreach (IWebElement act in actualArticles)
            {
                string newact = act.Text;
                jbk.Add(newact);
            }
            

            List<string> expectedArticles = new List<string>();
            expectedArticles.Add("The measure passed in both chambers of Congress and now heads to President Biden's desk.");
            expectedArticles.Add("The measure passed in both chambers of Congress and now heads to President Biden's desk.");
            expectedArticles.Add("With bodies decapitated or dismembered, it could take days for the victims to be identified.");
            expectedArticles.Add("Ex-British police officer Wayne Couzens will never be released after his kidnap, rape and murder of London woman Sarah Everard.");
            expectedArticles.Add("Irmgard Furchner - accused of complicity in more than 11,000 murders - tried to flee on Thursday.");
            expectedArticles.Add("The former French president is found guilty of illegally funding his unsuccessful 2012 re-election.");
            expectedArticles.Add("Democrat Cori Bush told her story for the first time in a House hearing about reproductive rights.");
            expectedArticles.Add("The Colombian singer was walking in a park in Barcelona, Spain, with her son when the animals struck.");
            expectedArticles.Add("Boris Johnson says leaders' 'reckless actions' created the climate crisis and now is the time to act.");
            expectedArticles.Add("The man took his life before he could be questioned about the case and provide a DNA sample.");
            expectedArticles.Add("The singer reveals she was molested by a family member when she was a child at music school.");
            expectedArticles.Add("The expulsions follow concerns raised by the UN about aid reaching the war-torn Tigray region.");
            expectedArticles.Add("The singer reveals she was molested by a family member when she was a child at music school.");
            expectedArticles.Add("The expulsions follow concerns raised by the UN about aid reaching the war-torn Tigray region.");
            expectedArticles.Add("A Turkish man joined a search party without realising he was the person they were looking for.");
            expectedArticles.Add("The indebted Chinese property giant is said to have missed more payments to overseas investors.");
            expectedArticles.Add("Facebook's global head of safety, Antigone Davis, testifies to the US Senate, about child protection.");

           Assert.AreEqual(expectedArticles[0], jbk[0]);
            driver.Quit();
        }

        [TestMethod]
        public void TestMethod3()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");

            Thread.Sleep(600);
            IWebElement newsButton = driver.FindElement(By.XPath("//a[text()='News']"));
            newsButton.Click();

            
            IWebElement category = driver.FindElement(By.XPath("//span[text()='Latin America & Caribbean']"));
            string searchRequest = category.Text;

            
            IWebElement searchField = driver.FindElement(By.XPath("//input[@id='orb-search-q']"));
            searchField.SendKeys(searchRequest);

            Thread.Sleep(200);
            IWebElement searchButton = driver.FindElement(By.XPath("//button[@id='orb-search-button']"));
            searchButton.Click();

            string expectedTest3 = "Afternoon Concert: Proms 2021: Nubya Garcia";
           
            var cc = driver.FindElements(By.XPath("//p[@class='ssrcss-7sxcrr-PromoHeadline e1f5wbog4']/span"));
            string actualTest3 = cc[0].Text;
            Assert.AreEqual(expectedTest3,actualTest3);
            
            driver.Quit();
        }

        [TestMethod]
        public void TestMethod4()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");

            Thread.Sleep(300);
            IWebElement newsButton = driver.FindElement(By.XPath("//a[text()='News']"));
            newsButton.Click();

            var coronavirusButton = driver.FindElements(By.XPath("//a[@href='/news/coronavirus']"));
            coronavirusButton[0].Click();

            var yourCoronavirusStoryButton = driver.FindElements(By.XPath("//a[@href='/news/have_your_say']"));
            yourCoronavirusStoryButton[0].Click();

            IWebElement shareNewsButton = driver.FindElement(By.XPath("//a[@href='/news/10725415']"));
            shareNewsButton.Click();

            IWebElement tellUsYourStoryField = driver.FindElement(By.XPath("//textarea[@aria-label='Tell us your story. ']"));
            tellUsYourStoryField.SendKeys("Trying");

            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            //Thread.Sleep(200);
            IWebElement nameField = driver.FindElement(By.XPath("//input[@aria-label='Name']"));
            nameField.SendKeys("Asya");

            IWebElement locatiionField = driver.FindElement(By.XPath("//input[@aria-label='Location ']"));
            locatiionField.SendKeys("Kyiv");

            IWebElement numberField = driver.FindElement(By.XPath("//input[@aria-label='Contact number ']"));
            numberField.SendKeys("88005553535");

            //var checkbox = driver.FindElements(By.XPath("//input[@type='checkbox']"));
            //checkbox[0].Click();
            //checkbox[1].Click();

            IWebElement submitButton = driver.FindElement(By.XPath("//button[@class='button']"));
            submitButton.Click();

            string expectedTest4 = "must be accepted";
            var resultXpath = driver.FindElements(By.XPath("//div[@class='input-error-message']"));
            string actualTest4 = resultXpath[1].Text;
            Assert.AreEqual(expectedTest4,actualTest4);

            driver.Quit();
        }
        [TestMethod]
        public void TestMethod5()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");

            Thread.Sleep(300);
            IWebElement newsButton = driver.FindElement(By.XPath("//a[text()='News']"));
            newsButton.Click();

            var coronavirusButton = driver.FindElements(By.XPath("//a[@href='/news/coronavirus']"));
            coronavirusButton[0].Click();

            var yourCoronavirusStoryButton = driver.FindElements(By.XPath("//a[@href='/news/have_your_say']"));
            yourCoronavirusStoryButton[0].Click();

            IWebElement shareNewsButton = driver.FindElement(By.XPath("//a[@href='/news/10725415']"));
            shareNewsButton.Click();

            IWebElement tellUsYourStoryField = driver.FindElement(By.XPath("//textarea[@aria-label='Tell us your story. ']"));
            tellUsYourStoryField.SendKeys("Trying");

            IWebElement submitButton = driver.FindElement(By.XPath("//button[@class='button']"));
            submitButton.Click();

            string expectedTest5 = "Name can't be blank";
            var resultXpath = driver.FindElements(By.XPath("//div[@class='input-error-message']"));
            string actualTest5 = resultXpath[0].Text;
            Assert.AreEqual(expectedTest5, actualTest5);

            driver.Quit();
        }
        [TestMethod]
        public void TestMethod6()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com");

            Thread.Sleep(300);
            IWebElement newsButton = driver.FindElement(By.XPath("//a[text()='News']"));
            newsButton.Click();

            var coronavirusButton = driver.FindElements(By.XPath("//a[@href='/news/coronavirus']"));
            coronavirusButton[0].Click();

            var yourCoronavirusStoryButton = driver.FindElements(By.XPath("//a[@href='/news/have_your_say']"));
            yourCoronavirusStoryButton[0].Click();

            IWebElement shareNewsButton = driver.FindElement(By.XPath("//a[@href='/news/10725415']"));
            shareNewsButton.Click();

            IWebElement nameField = driver.FindElement(By.XPath("//input[@aria-label='Name']"));
            nameField.SendKeys("Asya");

            IWebElement submitButton = driver.FindElement(By.XPath("//button[@class='button']"));
            submitButton.Click();

            string expectedTest6 = "can't be blank";
            var resultXpath = driver.FindElements(By.XPath("//div[@class='input-error-message']"));
            string actualTest6 = resultXpath[0].Text;
            Assert.AreEqual(expectedTest6, actualTest6);

            driver.Quit();
        }
    }
}
